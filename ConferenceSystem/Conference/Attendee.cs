﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conference
{

    /* Conference program created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  Attendee class will use get/set methods for the Variables together with a GetCost method to calculate the amount the Atendee should pay.  
    *  
    *  Buttons will be shown at the bottom of the GUI (Set, Clear, Get, Invoice & Certificate)
    *  
    *  Last Updated 17/10/2016 21:47pm
    */

    public class Attendee : Person // Attendee inherits firstname & secondname from Person
    {
        private int attendeeref;

        public Attendee() {} // Default constructor

        public int AttendeeRef // Attendee Reference
        {
            get
            {
                return attendeeref;
            }
            set
            {
                if (value < 40000
                    || value > 60000) // Attendee Ref data validation using exception (if lower than 40000 or higher than 60000)
                {
                    throw new ArgumentException("Your attendee reference must be between 40000 and 60000"); // Will throw this message if attendee reference is below 40,000 or above 60,000
                }
                else
                {
                    attendeeref = value;

                }
            }
        }
        public string ConferenceName { get; set; } // Conference Name with auto get & set
        public string RegistrationType { get; set; } // Registration Type with auto get & set
        public string Paid { get; set; } // Paid (Yes or No) with auto get & set
        public string Presenter { get; set; } // Presenter (Yes or No) with auto get & set
        public string PaperTitle { get; set; } // Paper Title being presented with auto get & set
         
        public int GetCost() // GetCost method for returning the cost based upon the Registration Type
        {
            int cost = 0;
            
            if (RegistrationType == "Full") // If full then cost is £500
            {
                cost = 500;
            }
            else if (RegistrationType == "Student") // If student then cost is £300
            {
                cost = 300;
            }
            else if (RegistrationType == "Organiser") // If organiser then cost is £0
            {
                cost = 0;
            }

            if (Presenter == "Yes" // If the Attendee is a presenter then 10% discount
                && RegistrationType != "Organiser") // Except for organisers
            {
                cost = cost / 100 * 90;
            }

            return cost;
        }
    }
}
