﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Conference
{
    /* Conference program created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This GUI (MainWindow) will allow the user to enter their details into the relevent fields.
    *  
    *  Buttons will be shown at the bottom of the GUI (Set, Clear, Get, Invoice & Certificate)
    *  
    *  Last Updated 17/10/2016 21:47pm
    */

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        Attendee person1 = new Attendee(); // New Attdendee object
        Institution instit1 = new Institution(); // New Instituion object

        public void setBtn_Click(object sender, RoutedEventArgs e) // When clicked, values contained in text/combo boxes will set the Attendee class properties
        {
            if (firstnameBox.Text == String.Empty ||         // Initial validation. Ensures no mandatory strings are empty prior to properties being set.
                surnameBox.Text == String.Empty ||
                attendeeBox.Text == String.Empty ||
                conferenceBox.Text == String.Empty ||
                regBox.Text == String.Empty ||
                paidBox.Text == String.Empty ||
                presenterBox.Text == String.Empty)
            {
                MessageBox.Show("Some information is missing please check your details"); // Message box shown if initial strings are empty
            }
            else // If strings are full then set the following properties
            {
                person1.FirstName = firstnameBox.Text;
                person1.SecondName = surnameBox.Text;

                try
                {
                    person1.AttendeeRef = Int32.Parse(attendeeBox.Text);
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);// Exception will be thrown if attendee ref entered is below 40,000 or above 60,000

                }

                instit1.InstitutionName = institutionBox.Text;
                instit1.InstitutionAddress = institutionAddBox.Text;
                person1.ConferenceName = conferenceBox.Text;
                person1.RegistrationType = regBox.Text; 
                person1.Paid = paidBox.Text; 
                person1.Presenter = presenterBox.Text; 

                if ( paperBox.Text == String.Empty
                    && presenterBox.Text == "Yes") // Message box will be shown if Presenter is selected but paper title is missing
                {
                    MessageBox.Show("Presenters must include a paper title");
                }
                else if (paperBox.Text != String.Empty
                    && presenterBox.Text == "No") // Message box will be shown if paper title is entered but attendee is now a Presenter
                {
                    MessageBox.Show("You are not a presenter. Please remove paper title");
                }
                else
                {
                 person1.PaperTitle = paperBox.Text;
                }
            }
        }

        private void clearBtn_Click(object sender, RoutedEventArgs e) // When clicked, removes string contents of text/combo boxes
        {
            firstnameBox.Text = String.Empty;
            surnameBox.Text = String.Empty;
            attendeeBox.Text = String.Empty;
            institutionBox.Text = String.Empty;
            institutionAddBox.Text = String.Empty;
            conferenceBox.Text = String.Empty;
            regBox.Text = String.Empty;
            paidBox.Text = String.Empty;
            presenterBox.Text = String.Empty;
            paperBox.Text = String.Empty;
        }

        public void getBtn_Click(object sender, RoutedEventArgs e) // When clicked, properties contained in the Attendee class method will update the text/combo boxes
        {
            firstnameBox.Text = person1.FirstName;
            surnameBox.Text = person1.SecondName;
            attendeeBox.Text = Convert.ToString(person1.AttendeeRef);
            conferenceBox.Text = person1.ConferenceName;
            regBox.Text = person1.RegistrationType;
            paidBox.Text = person1.Paid;
            presenterBox.Text = person1.Presenter;
            paperBox.Text = person1.PaperTitle;
            institutionBox.Text = instit1.InstitutionName;
            institutionAddBox.Text = instit1.InstitutionAddress;
        }

        public void invoiceBtn_Click(object sender, RoutedEventArgs e) // When clicked, will show an invoice for the attendee
        {
            Invoice newWin = new Invoice();
            newWin.invoiceName.Text = firstnameBox.Text + " " + surnameBox.Text;
            newWin.invoiceInstitution.Text = institutionBox.Text;
            newWin.invoiceConference.Text = conferenceBox.Text;
            newWin.regtypeInvoice.Text = regBox.Text;

            int amountValue = person1.GetCost(); // Calls GetCost method from Attendee class and sets amount on the Invoice Window
            newWin.amountInvoice.Text = Convert.ToString(amountValue);

            newWin.paidInvoice.Text = paidBox.Text;
            newWin.ShowDialog();
        }

        public void certBtn_Click(object sender, RoutedEventArgs e) // When clicked, will show a certificate for the attendee
        {
            Certificate newWin = new Certificate();

            if (person1.Presenter == "Yes") // If attendee is a presenter show this message
            {
                newWin.certText.Text = "This is to certify that " + person1.FirstName + " " + person1.SecondName + " attended " + person1.ConferenceName + " and presented a paper entitled, " + person1.PaperTitle + ".";
            }
            else // if attendee is NOT a presenter then show this message
            {
                newWin.certText.Text = "This is to certify that " + person1.FirstName + " " + person1.SecondName + " attended " + person1.ConferenceName;
            }
            newWin.ShowDialog();
        }
    }
}
