﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conference
{

    /* Conference program created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This Institution class will store the instituion name & institution address associated with the Attendee.
    *   
    *  Last Updated 17/10/2016 21:47pm
    */

    public class Institution
    {
        public string InstitutionName { get; set; } // auto get & set for Institution Name
        public string InstitutionAddress { get; set; } // auto get & set for Institution Address
    }
}
