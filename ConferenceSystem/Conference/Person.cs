﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conference
{
    /* Conference program created by Ben Wilkes of Edinburgh Napier University (matric number: 40278114) 
    *  
    *  This person class will store the first & last name of the attendee and will act as a parent class to the Attendee class.  
    *   
    *  Last Updated 17/10/2016 21:47pm
    */

    public class Person
    {
        public string FirstName { get; set; } // auto get set for First Name
        public string SecondName { get; set; } // auto get set for Second Name
    }
}
